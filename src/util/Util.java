package util;

import game.Point;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;
import java.util.Random;

public class Util {
    public static final Random random = new Random();

    public static final DecimalFormat decimalFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.GERMAN));

    public static Point randomCirclePoint(double radius){
        double x, y;
        do {
            x = random.nextDouble() * 2*radius - radius;
            y = random.nextDouble() * 2*radius - radius;
        } while (x*x + y*y > radius*radius);
        return new Point(x, y);
    }

    public static double fitInRange(double value, double min, double max){
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }

    public static double stringToDouble(String text, double original, double min, double max){
        double newValue;
        try{
            newValue = Util.fitInRange(decimalFormat.parse(text).doubleValue(), min, max);
        } catch (ParseException e){
            newValue = original;
        }
        return newValue;
    }

    public static double distPointLine(double a1, double a2, double angle, double p1, double p2){
        angle = Math.toRadians(angle);
        return Math.abs(Math.cos(angle)*(p2-a2) - Math.sin(angle)*(p1-a1));
    }
}
