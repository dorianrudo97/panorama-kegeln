package game;

public class GameConfig {
    private final double radius;
    private final int cones;
    private final PlayerType player1, player2;

    public GameConfig(double radius, int cones, PlayerType player1, PlayerType player2) {
        this.radius = radius;
        this.cones = cones;
        this.player1 = player1;
        this.player2 = player2;
    }

    public double getRadius() {
        return radius;
    }

    public int getCones() {
        return cones;
    }

    public PlayerType getPlayer1() {
        return player1;
    }

    public PlayerType getPlayer2() {
        return player2;
    }
}
