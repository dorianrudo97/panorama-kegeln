package game;

import game.ki.AI;
import util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

//Class for managing the game
public class Game {
    private GameConfig config;
    private int move = 1, round = 1;

    private int score1 = 0;
    private int score2 = 0;
    private boolean player1Playing = true;
    private AI player1 = null, player2 = null;

    private ArrayList<Point> conesUp = new ArrayList<>(), conesDown = new ArrayList<>(), newConesDown = new ArrayList<>();

    private double aimX = 0, aimY = 0, aimAngle = 0;
    private boolean abortRound = false;

    public Game(GameConfig config) {
        this.config = config;
        placeCones();
        player1 = config.getPlayer1().newAI(config);
        player2 = config.getPlayer2().newAI(config);
        calculateNextMove();
    }

    //Should be called at the beginning of each round
    private void placeCones(){
        conesUp.clear();
        conesDown.clear();
        newConesDown.clear();
        for (int i = 0; i < config.getCones(); ++i)
            conesUp.add(Util.randomCirclePoint(config.getRadius()));
    }

    //Returns unmodifiable view of the List so that the AI cannot change anything
    public List<Point> getConesUp(){
        return Collections.unmodifiableList(conesUp);
    }

    public List<Point> getConesDown(){
        return Collections.unmodifiableList(conesDown);
    }

    public List<Point> getNewConesDown() {
        return Collections.unmodifiableList(newConesDown);
    }

    public GameConfig getConfig() {
        return config;
    }

    //Methods to input human interaction
    public void setHumanX(double x) {
        aimX = x;
        recalculateNewConesDown();
    }

    public void setHumanY(double y) {
        aimY = y;
        recalculateNewConesDown();
    }

    public void setHumanAngle(double angle) {
        aimAngle = angle;
        recalculateNewConesDown();
    }

    public void setHumanWillAbortRound(boolean abortRound) {
        this.abortRound = abortRound;
    }

    public double getAimX() {
        return aimX;
    }

    public double getAimY() {
        return aimY;
    }

    public double getAimAngle() {
        return aimAngle;
    }

    public boolean willAbortRound() {
        return abortRound;
    }

    public int getScore1() {

        return score1;
    }

    public int getScore2() {
        return score2;
    }

    public boolean isPlayer1Playing() {
        return player1Playing;
    }

    public int getMove() {
        return move;
    }

    public boolean isHumanPlaying() {
        return (player1Playing && config.getPlayer1() == PlayerType.HUMAN)
                || (!player1Playing && config.getPlayer2() == PlayerType.HUMAN);
    }

    //Calculate the next move for the AI if it is to play
    private void calculateNextMove(){
        if (!isHumanPlaying()){
            AI player = player1Playing ? player1 : player2;
            player.calculateMove(getConesUp());
            aimX = player.getAimX();
            aimY = player.getAimY();
            aimAngle = player.getAimAngle();
            abortRound = player.willAbortRound();
        }
        recalculateNewConesDown();
    }

    //Set newConesDown to the list of all cones that will fall with this throw
    private void recalculateNewConesDown(){
        newConesDown.clear();
        newConesDown.addAll(conesUp.stream()
                .filter(p -> Util.distPointLine(aimX, aimY, aimAngle, p.getX(), p.getY()) <= 1)
                .collect(Collectors.toList()));
    }

    //Let the ball be thrown
    public void throwBall(){
        ++move;

        //update score
        if (player1Playing)
            score1 += newConesDown.size();
        else
            score2 += newConesDown.size();

        //remove all cones that have been knocked over
        conesUp.removeIf(p -> Util.distPointLine(aimX, aimY, aimAngle, p.getX(), p.getY()) <= 1);
        conesDown.addAll(newConesDown);
        newConesDown.clear();

        //Start a new round if necessary
        if (conesUp.isEmpty() || (abortRound && !player1Playing)){
            ++round;
            placeCones();
            recalculateNewConesDown();
            player1Playing = true;
        } else {
            player1Playing = !player1Playing;
        }

        calculateNextMove();
    }

    public int getRound() {
        return round;
    }
}
