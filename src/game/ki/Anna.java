package game.ki;

import game.GameConfig;
import game.Point;
import util.Util;

import java.util.List;
import java.util.stream.Collectors;

public class Anna extends AI {
    public Anna(GameConfig config) {
        super(config);
    }

    //Calculate how many cones would be thrown down
    private static int throwScore(List<Point> cones, double x, double y, double angle) {
        int count = 0;
        for (Point p : cones)
            if (Util.distPointLine(x, y, angle, p.getX(), p.getY()) <= 1)
                ++count;
        return count;
    }

    @Override
    public void calculateMove(List<Point> conesUp) {
        double bestX = 0, bestY = 0, bestAngle = 0, radius = config.getRadius();
        int best = 0;

        //Bruteforce find the best possible throw
        for (double posAngle = 0; posAngle < 360; posAngle += 1) {
            double rad = Math.toRadians(posAngle);
            double x = Math.sin(rad) * radius;
            double y = Math.cos(rad) * radius;
            for (double angle = 0; angle < 180; angle += 1) {
                int score = throwScore(conesUp, x, y, angle);
                if (score > best) {
                    bestX = x;
                    bestY = y;
                    bestAngle = angle;
                    best = score;
                }
            }
        }

        aimAngle = bestAngle;
        aimX = bestX;
        aimY = bestY;

        //Cones that would be remaining
        List<Point> stillUp = conesUp.stream()
                .filter(p -> Util.distPointLine(aimX, aimY, aimAngle, p.getX(), p.getY()) > 1)
                .collect(Collectors.toList());

        //Find average number of cones that would be thrown down using a random throw
        int count = 0, total = 0;
        for (double posAngle = 0; posAngle < 360; posAngle += 1) {
            double rad = Math.toRadians(posAngle);
            for (double angle = 0; angle <= 180; angle += 1) {
                total += throwScore(stillUp, Math.sin(rad) * radius, Math.cos(rad) * radius, angle);
                ++count;
            }
        }

        //Abort the round if Randy would throw down half of the remaining cones or more on average
        abortRound = ((double)total / count) / stillUp.size() > 0.5;
    }
}
