package game.ki;

import game.GameConfig;
import game.Point;
import util.Util;

import java.util.List;

public abstract class AI {
    double aimX, aimY, aimAngle;
    boolean abortRound;
    GameConfig config;

    public AI(GameConfig config) {
        this.config = config;
    }

    public double getAimX() {
        return Util.fitInRange(aimX, -config.getRadius(), config.getRadius());
    }

    public double getAimY() {
        return Util.fitInRange(aimY, -config.getRadius(), config.getRadius());
    }

    public boolean willAbortRound(){
        return abortRound;
    }

    public double getAimAngle() {
        return Util.fitInRange(aimAngle, 0, 360);
    }

    public abstract void calculateMove(List<Point> conesUp);
}
