package game.ki;

import game.GameConfig;
import game.Point;
import util.Util;

import java.util.List;

public class Randy extends AI {

    public Randy(GameConfig config){
        super(config);
    }

    @Override
    public void calculateMove(List<Point> conesUp) {
        Point p = Util.randomCirclePoint(config.getRadius());
        aimX = p.getX();
        aimY = p.getY();
        aimAngle = 360 * Util.random.nextDouble();
    }
}
