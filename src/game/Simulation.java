package game;

public class Simulation {
    Game game;
    int rounds, repetitions, sum1 = 0, sum2 = 0, wins1 = 0, wins2 = 0;
    GameConfig config;

    public Simulation(int rounds, int repetitions, int cones, double radius) {
        this.rounds = rounds;
        this.repetitions = repetitions;
        config = new GameConfig(radius, cones, PlayerType.RANDY, PlayerType.ANNA);
    }

    //Run 'repetitions' games with 'rounds' rounds each and print out the results
    public void run(){
        System.out.println("Randy | Anna");
        for (int i = 1; i <= repetitions; ++i){
            //Create new instance of game and call throwBall until the requested round count is reached
            game = new Game(config);
            while(game.getRound() <= rounds)
                game.throwBall();
            //Keep track of total score over the rounds
            sum1 += game.getScore1();
            sum2 += game.getScore2();
            //Count wins
            if (game.getScore1() > game.getScore2())
                ++wins1;
            else if (game.getScore1() < game.getScore2())
                ++wins2;
            System.out.println(game.getScore1() + " " + game.getScore2());
        }
        System.out.println("Average Randy: " + (sum1 / (double)rounds));
        System.out.println("Average Anna: " + (sum2 / (double)rounds));
        System.out.println("Wins Randy: " + wins1);
        System.out.println("Wins Anna: " + wins2);
    }
}
