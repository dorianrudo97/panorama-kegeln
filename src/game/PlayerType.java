package game;

import game.ki.Anna;
import game.ki.AI;
import game.ki.Randy;

//PlayerType can be used to represent the different playing modes and can also create the appropriate AI
public enum PlayerType {
    HUMAN("Mensch"),
    RANDY("Randy"),
    ANNA("Anna");

    private final String name;

    PlayerType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public AI newAI(GameConfig config){
        switch (this){
            case HUMAN: return null;
            case RANDY: return new Randy(config);
            case ANNA: return new Anna(config);
        }
        return null;
    }
}
