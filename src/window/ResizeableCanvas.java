package window;

import javafx.beans.InvalidationListener;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Region;


class ResizeableCanvas extends Canvas {

    public void addDraw(InvalidationListener listener){
        widthProperty().addListener(listener);
        heightProperty().addListener(listener);
    }

    public void bindRegion(Region region){
        widthProperty().bind(region.widthProperty());
        heightProperty().bind(region.heightProperty());
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }

    @Override
    public double prefHeight(double width) {
        return getHeight();
    }
}