package window;

import game.Game;
import game.GameConfig;
import game.PlayerType;
import game.Point;
import javafx.application.Application;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import util.Util;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public class Window extends Application {
    static final Color selectionColor = new Color(0.3, 0.3, 0.3, 0.3);
    ResizeableCanvas canvas;
    Label statusLabel;
    TextField textFieldX, textFieldY, textFieldAngle;
    Slider sliderAngle;
    CheckBox checkBoxAbort;

    SimpleBooleanProperty interactionDisabled = new SimpleBooleanProperty(true), notPlaying = new SimpleBooleanProperty(true);
    double aimX = 0, aimY = 0, aimAngle = 0;
    double drawRadius = 100, radius = 2;

    Game game = null;

    //Update the values for aim, x and y
    private void setAimX(double x){
        aimX = x;
        textFieldX.setText(Util.decimalFormat.format(x));
        if (game.isHumanPlaying())
            game.setHumanX(x);
        draw();
    }

    private void setAimY(double y){
        aimY = y;
        textFieldY.setText(Util.decimalFormat.format(y));
        if (game.isHumanPlaying())
            game.setHumanY(y);
        draw();
    }

    private void setAimAngle(double angle){
        aimAngle = angle;
        textFieldAngle.setText(Util.decimalFormat.format(angle));
        sliderAngle.setValue(angle);
        if (game.isHumanPlaying())
            game.setHumanAngle(angle);
        draw();
    }

    //Configure a new game
    private GameConfig newGameDialog(){
        Dialog<GameConfig> dialog = new Dialog<>();
        dialog.setTitle("Neues Spiel");
        dialog.setHeaderText("Konfiguriere die Einstellungen für das neue Spiel");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        Node okButton = dialog.getDialogPane().lookupButton(ButtonType.OK);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10));

        grid.add(new Label("Spieler 1 (beginnt immer):"), 0, 0);
        ChoiceBox<PlayerType> player1 = new ChoiceBox<>(FXCollections.observableArrayList(PlayerType.values()));
        player1.setValue(PlayerType.RANDY);
        grid.add(player1, 1, 0);

        grid.add(new Label("Spieler 2 (kann abbrechen):"), 0, 1);
        ChoiceBox<PlayerType> player2 = new ChoiceBox<>(FXCollections.observableArrayList(PlayerType.values()));
        player2.setValue(PlayerType.ANNA);
        grid.add(player2, 1, 1);

        grid.add(new Label("Kegel (1..1000):"), 0, 2);
        TextField conesTextField = new TextField("20");
        conesTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            int cones = 0;
            try {
                cones = Integer.parseInt(newValue);
            } catch (NumberFormatException e) {
            }
            okButton.setDisable(cones <= 0 || cones > 1000);
        });
        grid.add(conesTextField, 1, 2);

        grid.add(new Label("Radius (1,0..20,0):"), 0, 3);
        TextField radiusTextField = new TextField("2");
        radiusTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            double radius = 0;
            try {
                radius = Util.decimalFormat.parse(newValue).doubleValue();
            } catch (ParseException e) {
            }
            okButton.setDisable(radius < 1 || radius > 20);
        });
        grid.add(radiusTextField, 1, 3);

        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(button -> {
            try {
                if (button == ButtonType.OK)
                    return new GameConfig(Util.decimalFormat.parse(radiusTextField.getText()).doubleValue(),
                            Integer.parseInt(conesTextField.getText()), player1.getValue(), player2.getValue());
            } catch (Exception e) {
                return null;
            }
            return null;
        });

        Optional<GameConfig> result = dialog.showAndWait();
        return result.isPresent() ? result.get() : null;
    }

    //Start the Window
    @Override
    public void start(Stage stage){
        SplitPane splitPane = new SplitPane();

        Pane pane = new Pane();
        canvas = new ResizeableCanvas();
        pane.getChildren().add(canvas);
        canvas.bindRegion(pane);
        canvas.addDraw(evt -> draw());
        canvas.setCursor(Cursor.CROSSHAIR);

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(5));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setAlignment(Pos.CENTER);
        SplitPane.setResizableWithParent(gridPane, false);

        Button buttonNewGame = new Button("Neues Spiel");
        buttonNewGame.setOnAction(event -> {
            GameConfig config = newGameDialog();
            if (config != null)
                newGame(config);
        });
        gridPane.add(buttonNewGame, 0, 0, 2, 1);
        GridPane.setHalignment(buttonNewGame, HPos.CENTER);

        gridPane.add(new Separator(), 0, 1, 2, 1);

        Label labelAngle = new Label("Winkel:");
        labelAngle.disableProperty().bind(interactionDisabled);
        GridPane.setHalignment(labelAngle, HPos.RIGHT);
        gridPane.add(labelAngle, 0, 2);

        sliderAngle = new Slider(0, 360, 0);
        sliderAngle.disableProperty().bind(interactionDisabled);

        textFieldAngle = new TextField("0");
        textFieldAngle.disableProperty().bind(interactionDisabled);
        textFieldAngle.setMaxWidth(100);
        textFieldAngle.setOnAction(event -> gridPane.requestFocus());
        textFieldAngle.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) setAimAngle(Util.stringToDouble(textFieldAngle.getText(), aimAngle, 0, 360));
        });
        gridPane.add(textFieldAngle, 1, 2);

        sliderAngle.valueProperty().addListener((observable, oldValue, newValue) -> {
            textFieldAngle.setText(Util.decimalFormat.format(newValue));
            aimAngle = newValue.doubleValue();
            game.setHumanAngle(aimAngle);
            draw();
        });
        gridPane.add(sliderAngle, 0, 3, 2, 1);

        gridPane.add(new Separator(), 0, 4, 2, 1);

        Label labelX = new Label("X-Position:");
        labelX.disableProperty().bind(interactionDisabled);
        GridPane.setHalignment(labelX, HPos.RIGHT);
        gridPane.add(labelX, 0, 5);

        textFieldX = new TextField("0");
        textFieldX.disableProperty().bind(interactionDisabled);
                textFieldX.setMaxWidth(100);
        textFieldX.setOnAction(event -> gridPane.requestFocus());
        textFieldX.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) setAimX(Util.stringToDouble(textFieldX.getText(), aimX, -radius, radius));
        });
        gridPane.add(textFieldX, 1, 5);

        Label labelY = new Label("Y-Position:");
        labelY.disableProperty().bind(interactionDisabled);
        GridPane.setHalignment(labelY, HPos.RIGHT);
        gridPane.add(labelY, 0, 6);

        textFieldY = new TextField("0");
        textFieldY.disableProperty().bind(interactionDisabled);
        textFieldY.setMaxWidth(100);
        textFieldY.setOnAction(event -> gridPane.requestFocus());
        textFieldY.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) setAimY(Util.stringToDouble(textFieldY.getText(), aimY, -radius, radius));
        });
        gridPane.add(textFieldY, 1, 6);

        canvas.setOnMouseClicked(event -> {
            if (interactionDisabled.getValue()) return;
            setAimX(aimX = Util.fitInRange((event.getX() - canvas.getWidth() / 2) / drawRadius * radius, -radius, radius));
            setAimY(Util.fitInRange((event.getY() - canvas.getHeight() / 2) / drawRadius * radius, -radius, radius));
        });

        gridPane.add(new Separator(), 0, 7, 2, 1);

        Button buttonThrow = new Button("Wurf");
        buttonThrow.setOnAction(event -> throwBall());
        buttonThrow.disableProperty().bind(notPlaying);
        gridPane.add(buttonThrow, 0, 8, 2, 1);
        GridPane.setHalignment(buttonThrow, HPos.CENTER);

        checkBoxAbort = new CheckBox("Runde abbrechen");
        checkBoxAbort.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (game.isHumanPlaying())
                game.setHumanWillAbortRound(newValue);
        });
        gridPane.add(checkBoxAbort, 0, 9, 2, 1);
        GridPane.setHalignment(checkBoxAbort, HPos.CENTER);

        gridPane.add(new Separator(), 0, 10, 2, 1);

        statusLabel = new Label("Starte ein neues Spiel, um zu beginnen.");
        statusLabel.setWrapText(true);
        gridPane.add(statusLabel, 0, 11, 2, 1);

        splitPane.getItems().add(gridPane);
        splitPane.getItems().add(pane);
        splitPane.setDividerPosition(0, 0.3);

        ColumnConstraints constraints = new ColumnConstraints();
        constraints.setHgrow(Priority.ALWAYS);
        constraints.setPercentWidth(50);
        gridPane.getColumnConstraints().addAll(constraints, constraints);

        stage.setTitle("Panorama Kegeln");
        stage.setScene(new Scene(splitPane, 640, 480));
        stage.show();
    }

    //Draw all points either filled or stroked
    private static void drawPoints(List<Point> points, double scale, GraphicsContext gc, boolean fill){
        for (Point p : points)
            if (fill) gc.fillOval(p.getX()*scale-2, p.getY()*scale-2, 4, 4);
            else gc.strokeOval(p.getX()*scale-2, p.getY()*scale-2, 4, 4);
    }

    //Update the visual representation of the playing field
    private void draw(){
        double width = canvas.getWidth(), height = canvas.getHeight();
        //Position of the center
        double mx = width / 2, my = height / 2;
        //Radius on screen
        drawRadius = Math.min(width, height)/2 - 2;
        //diameter on screen, ball radius on screen and scaling between screen and ingame coordinates
        double diameter = drawRadius * 2, ballRadius = drawRadius / radius, scale = drawRadius / radius;
        //length of the marker indicating the selected position, of the ball throwing box
        double marker = drawRadius / 30, length = 1.5 * Math.max(width, height);

        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, width, height);

        //Save the configuration of the GraphicsContext
        gc.save();

        //The center of the circle is now the new origin
        gc.translate(mx, my);
        gc.setFill(Color.WHITE);
        gc.fillOval(-drawRadius, -drawRadius, diameter, diameter);
        gc.strokeOval(-drawRadius, -drawRadius, diameter, diameter);

        gc.save();
        //Now translate to the aim position
        gc.translate(aimX * scale, aimY*scale);
        gc.rotate(aimAngle);
        gc.strokeLine(-length, 0, 1.5 * length, 0);
        gc.setFill(selectionColor);
        gc.fillRect(-length, -ballRadius, 2 * length, 2 * ballRadius);
        gc.rotate(90);
        gc.strokeLine(-marker, 0, marker, 0);
        gc.restore();

        //Draw the cones that are down already stroked, the threatened ones in red and the other ones in black
        if (game != null) {
            gc.setFill(Color.BLACK);
            drawPoints(game.getConesUp(), scale, gc, true);
            drawPoints(game.getConesDown(), scale, gc, false);
            gc.setFill(Color.RED);
            drawPoints(game.getNewConesDown(), scale, gc, true);
        }

        gc.restore();
    }

    //Responder for the throw button
    private void throwBall(){
        if (game.isHumanPlaying()){
            game.setHumanAngle(aimAngle);
            game.setHumanX(aimX);
            game.setHumanY(aimY);
            game.setHumanWillAbortRound(checkBoxAbort.isSelected());
        }
        game.throwBall();
        update();
    }

    //Update everything on screen
    private void update(){
        interactionDisabled.set(!game.isHumanPlaying());
        checkBoxAbort.setDisable(interactionDisabled.get() || game.isPlayer1Playing());
        if (!game.isHumanPlaying()){
            setAimAngle(game.getAimAngle());
            setAimX(game.getAimX());
            setAimY(game.getAimY());
            checkBoxAbort.setSelected(game.willAbortRound());
        } else {
            draw();
        }
        String scores = String.format("Spieler 1 (%s): %d\nSpieler 2 (%s): %d\n\nWurf %d, Runde %d\n",
                game.getConfig().getPlayer1(), game.getScore1(), game.getConfig().getPlayer2(), game.getScore2(), game.getMove(), game.getRound());
        String info = String.format("Spieler %s ist dran.\n%d Kegel in der Wurfbahn.",
                game.isPlayer1Playing() ? "1" : "2", game.getNewConesDown().size());
        statusLabel.setText(scores + info);
    }

    private void newGame(GameConfig config){
        game = new Game(config);
        radius = config.getRadius();
        notPlaying.set(false);
        update();
    }
}
