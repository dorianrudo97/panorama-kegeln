import game.Simulation;
import util.Util;
import window.Window;

public class Main {
    public static void main(String[] args) {
        if (args.length != 4){
            //Launch UI
            Window.launch(Window.class, args);
        } else {
            //Read parameters and run Simulation in Console
            double radius;
            int rounds, repetitions, cones;
            try {
                rounds = Integer.parseInt(args[0]);
                repetitions = Integer.parseInt(args[1]);
                cones = Integer.parseInt(args[2]);
                radius = Util.decimalFormat.parse(args[3]).doubleValue();
                if (rounds < 1 || repetitions < 1 || cones < 1 || radius < 1){
                    System.out.println("Please enter values that make sense.");
                    return;
                }
            } catch (Exception e){
                System.out.println("Please enter correct paremeter values like '10 10 20 2,0'");
                return;
            }
            Simulation simulation = new Simulation(rounds, repetitions, cones, radius);
            simulation.run();
        }
    }
}
